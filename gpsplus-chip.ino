/*

ArduinoGPS - A simple GPS device based on arduino

v 0.1 - Ivo Schooneman - ivo -at- schooneman.net

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <TinyGPS++.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SD.h>

#define DEBUG 1 // Set serial debuggin on/off

TinyGPSPlus gps;        // Initialize GPS

#define compas_id 0x1E  //0011110b, I2C 7bit address of HMC5883 compas

#define OLED_DC     6   // OLED Settings
#define OLED_CS     7
#define OLED_RESET  8
#define XPOS 0
#define YPOS 1
Adafruit_SSD1306 display(OLED_DC, OLED_RESET, OLED_CS);

int xPin = A1;    // Joystick
int yPin = A0;
int buttonPin = 5;
int xPosition = 0;
int yPosition = 0;
int buttonState = 0;

Sd2Card card;     // Setup the sd card
SdVolume volume;
SdFile root;
const int SDchipSelect = 8;
char track_filename[] = "track00.gpx";
File configFile;
int lastGPX=0;
int SD_avail=0;
String gpx_lasttime;
String gpx_curtime;

#define LOGO_width 128   // Boot-logo :)
#define LOGO_height 64
static const unsigned char PROGMEM LOGO_bits[] = {  // Logo displayed during startup
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfe,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xe7,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xc3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0x83,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xfe,0x1,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xfc,0x20,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xf8,0x70,0x7f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xf0,0xf8,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xc1,0xfc,0x1f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0x87,0xfe,0x1f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xf,0xfe,0xf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xfe,0x1f,0xff,0x7,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xfe,0x3f,0xff,0x83,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xfc,0x3f,0xff,0xc3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xfc,0x7f,0xff,0x87,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xfc,0x7f,0xff,0xf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xf8,0x7f,0xfc,0x1f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xf8,0xff,0xf8,0x7f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xf1,0xf0,0xff,0xf0,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xe0,0xf0,0x7f,0xe1,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xc0,0xf0,0x3f,0xc3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0x80,0x78,0x0,0x7,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0x0,0x3c,0x0,0x1f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xfe,0x0,0x1c,0x0,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xfe,0x0,0xe,0x1,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0x0,0xf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0x80,0x7,0xff,0xff,0xff,0xf9,0xff,0xff,0xfe,0xf,0x3,0xc1,0xff,
0xff,0xff,0xff,0xc0,0x3,0xff,0xff,0xff,0xf9,0xff,0xff,0xfc,0x7,0x0,0x8c,0xff,
0xff,0xff,0xff,0xe0,0x1,0xff,0xff,0xff,0xf9,0x9c,0xc3,0xf8,0xf3,0x3c,0x9f,0xff,
0xff,0xff,0xff,0xe0,0x0,0xff,0xff,0xff,0xf9,0x9d,0x81,0xf9,0xff,0x3c,0x87,0xff,
0xff,0xfe,0x0,0x70,0x0,0xff,0xff,0xff,0xf9,0x99,0x99,0xf9,0xc3,0x1,0xc0,0xff,
0xff,0xf8,0x0,0x38,0x0,0xff,0xff,0xff,0xf9,0xc9,0x3c,0xf9,0xc3,0x1,0xf8,0x7f,
0xff,0xf0,0x0,0x3c,0x1,0xff,0xff,0xff,0xf9,0xc9,0x3c,0xf9,0xf3,0x3f,0xbe,0x7f,
0xff,0xe0,0x0,0x1e,0x3,0xff,0xff,0xff,0xf9,0xc3,0x39,0xfc,0xf3,0x3f,0x9e,0x7f,
0xff,0xc3,0xfc,0xf,0x7,0xfd,0xff,0xff,0xf9,0xe3,0x99,0xfc,0x3,0x3f,0x80,0x7f,
0xff,0x87,0xfe,0xf,0xf,0xf8,0x7f,0xff,0xf9,0xe3,0xc3,0xfe,0xb,0x3f,0xc0,0xff,
0xfe,0xf,0xff,0x1f,0xff,0xf0,0x7f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xfc,0x1f,0xff,0x1f,0xff,0xe0,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xf8,0x7f,0xfe,0x1f,0xff,0xc1,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xf0,0xff,0xfe,0x3f,0xff,0x83,0xe1,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xc1,0xff,0xfc,0x3f,0xff,0x7,0xc3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xc3,0xff,0xfc,0x7f,0xfc,0xf,0x83,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xc1,0xff,0xfc,0x7f,0xf8,0x1f,0x7,0x87,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xe0,0xff,0xf8,0x7f,0xf0,0x3e,0xf,0x7,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xf0,0x7f,0xe0,0xff,0xf0,0x7c,0x1f,0xf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xf8,0x7f,0xc3,0xff,0xf1,0xf8,0x3e,0x1f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xf8,0x3f,0x87,0xff,0xff,0xf0,0x7c,0x1f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xfc,0x1f,0xf,0xff,0xff,0xc0,0xf8,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xfe,0xe,0x1f,0xff,0xff,0x81,0xf0,0x7f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0x0,0x3f,0xff,0xfe,0x7,0xe0,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0x80,0xff,0xff,0xfc,0xf,0xc1,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xc1,0xff,0xff,0xfe,0x3f,0x83,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xc3,0xff,0xff,0xfe,0xfe,0x7,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xe7,0xff,0xff,0xff,0xfc,0xf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xf0,0x1f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xc0,0x7f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0x80,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0x83,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfe
};

double lat, lng;
unsigned long fix_age, altitude, chars;
unsigned short sentences, failed_checksum;
int year;
byte month, day, hour, minute, second, hundredths;

String slat, slng;

void GPX_WRITE_TRACKLOG_HEADER(){
  if(SD_avail > 0){
    File gpx_track = SD.open(track_filename, FILE_WRITE); // Create file
    if(gpx_track){
      gpx_track.println("<?xml version=\"1.0\"?>");
      gpx_track.println("<gpx version=\"1.1\" creator=\"Ivo-gps\">");
      gpx_track.println("<trk>");
      gpx_track.println("<name>Recorded track</name>");
      gpx_track.println("<desc>Track recorded by Arduino GPS system.k</desc>");
      gpx_track.println("<trkseg>");  
      gpx_track.close();
    }
  }
}

void GPX_WRITE_TRACKLOG_POINT(String gps_lat, String gps_lng, String gps_time, String gps_ele, String gps_hdop){
  if(SD_avail > 0){
    File gpx_track = SD.open(track_filename, FILE_WRITE); // open file
    if(gpx_track){
      gpx_track.print("<trkpt lat=\"");
      gpx_track.print(gps_lat);
      gpx_track.print("\" lon=\"");
      gpx_track.print(gps_lng);
      gpx_track.println("\">");
      gpx_track.print("<ele>");
      gpx_track.print(gps_ele);
      gpx_track.println("</ele>");
      gpx_track.print("<time>");
      gpx_track.print(gps_time);
      gpx_track.println("</time>");
      gpx_track.print("<hdop>");
      gpx_track.print(gps_hdop);
      gpx_track.println("</hdop>");
      gpx_track.println("</trkpt>");
      gpx_track.close();
    }
  }
}

void GPX_WRITE_TRACKLOG_FOOTER(){
  if(SD_avail > 0){
    File gpx_track = SD.open(track_filename, FILE_WRITE); // open file
    if(gpx_track){
      gpx_track.println("</trkseg>");
      gpx_track.println("</trk>");
      gpx_track.println("</gpx>");
      gpx_track.close();
    }
  }
}

void READ_SETTINGS(){             // Read settings from SD card
  if(SD_avail > 0){
    char character;
    String configName;
    String configVal;
    configFile = SD.open("config.txt");
    if (configFile) {
      while (configFile.available()) {
        character = configFile.read();
        while((configFile.available()) && (character != '[')){
          character = configFile.read();
        }
        character = configFile.read();
        while((configFile.available()) && (character != '=')){
          configName = configName + character;
          character = configFile.read();
         }
        character = configFile.read();
        while((configFile.available()) && (character != ']')){
          configVal = configVal + character;
          character = configFile.read();
        }
        if(character == ']'){
          if(DEBUG){
            Serial.print("Name:");
            Serial.println(configName);
            Serial.print("Value :");
            Serial.println(configVal);
          }
          applySetting(configName,configVal);
          configName="";
          configVal="";
        }
      }
      configFile.close();
    }else{
      if(DEBUG){
        Serial.println("error opening config.txt");
      }
    }
  }
}

void WRITE_SETTINGS() {
  if(SD_avail > 0){
    SD.remove("config.txt"); // Delete the old One
    File configFile = SD.open("config.txt", FILE_WRITE); // Create new one
    if(configFile){
      configFile.print("[");
      configFile.print("lastGPX=");
      configFile.print(lastGPX);
      configFile.println("]");
      configFile.close(); // close the file:
      if(DEBUG){
        Serial.println("Writing done.");
      }
    }else{
      if(DEBUG){
        Serial.println("Writing failed.");
      }
    }
  }
}

void applySetting(String configName, String configVal) {
  if(configName == "lastGPX") {
    lastGPX=configVal.toInt();
  }
}
 
void COMPAS(){                    // Show heading
  int x,y,z;
  Wire.beginTransmission(compas_id);
  Wire.write(0x03); //select register 3, X MSB register
  Wire.endTransmission();
  Wire.requestFrom(compas_id, 6);
  if(6<=Wire.available()){
    x = Wire.read()<<8; //X msb
    x |= Wire.read(); //X lsb
    z = Wire.read()<<8; //Z msb
    z |= Wire.read(); //Z lsb
    y = Wire.read()<<8; //Y msb
    y |= Wire.read(); //Y lsb
  }
  
  // Hold the module so that Z is pointing 'up' and you can measure the heading with x&y
  // Calculate heading when the magnetometer is level, then correct for signs of axis.
  float heading = atan2(y,x);
  // Correct for when signs are reversed.
  if(heading < 0)
    heading += 2*PI;
    
  // Check for wrap due to addition of declination.
  if(heading > 2*PI)
    heading -= 2*PI;
   
  // Convert radians to degrees for readability.
  float headingDegrees = heading * 180/M_PI; 
  
  display.setCursor(0,8);
  display.print("Heading: ");
  display.println(headingDegrees);
}

void DISPLAY_TIME(){              // Show time
  //gps.crack_datetime(&year, &month, &day,    //Date/time cracking
  //&hour, &minute, &second, &hundredths, &fix_age);  
  display.setCursor(0,0);
  display.print("Time: ");
  if(hour<=8){
    display.print('0');
  };
  display.print(hour+1);
  display.print(":");
  if(minute<10){
    display.print('0');
  };
  display.print(minute);
  display.print(":");
  if(second<10){
    display.print('0');
  };
  display.println(second);
}

void MENU(){                      // The menu when joystick is pressed
  int menu=1;
  int selected=0;
 
  while(menu>0){
    xPosition = analogRead(xPin);
    yPosition = analogRead(yPin);
    buttonState = digitalRead(buttonPin);
    if(xPosition > 550){
      selected++;
    };
    if(xPosition < 480){
      selected--;
    };
    if(selected < 0) {
      selected = 0;
    };
    display.clearDisplay(); // Clear display
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    if(selected == 0){
      display.print("> ");
    };
    display.println("Menu");
    if(selected == 1){
      display.print("> ");
    };
    display.println("Go to");
    if(selected == 2){
      display.print("> ");
    };
    display.println("Shutdown");
    if(selected > 3){
      display.print("> ");
    };
    display.println("Exit");
    display.display();
    if(selected == 1 && buttonState == 0){
      menu = 0;
      SET_DESTINATION();
    };
    if(selected == 2 && buttonState == 0){
      menu = 0;
      SHUTDOWN();
    };
    if(selected > 3 && buttonState == 0){
      menu = 0;
    };
    delay(150);
  }
}

void SET_DESTINATION(){           // Submenu to set destination coordinates
  int menu=1;
  int x_select=0;
  int y_select=0;

  while(menu>0){
    xPosition = analogRead(xPin);
    yPosition = analogRead(yPin);
    buttonState = digitalRead(buttonPin);
    if(xPosition > 550){
      x_select++;
    };
    if(xPosition < 480){
      x_select--;
    };
    if(yPosition > 550){
      y_select++;
    };
    if(yPosition < 480){
      y_select--;
    };
    if(x_select < 0){
      x_select=0;
    };
    if(y_select < 0){
      y_select=0;
    };
    display.clearDisplay(); // Clear display
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.println("Set destination:");
    if(x_select > 2 && buttonState == 0){
      menu = 0; MENU();
    };
    if(x_select == 1){
      display.print("> ");
    };
    display.println("Lat:");
    if(x_select == 2){
      display.print("> ");
    };
    display.println("Lng:");
    if(x_select > 2){
      display.print("> ");
    };
    display.println("Exit");
    display.display();
    delay(150);
  }
}

void(* resetFunc) (void) = 0; //declare reset function @ address 0

void SHUTDOWN(){                  // End gpx file, shutdown all modules
  GPX_WRITE_TRACKLOG_FOOTER();
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Shutting");
  display.println("down...");
  display.display();
  delay(5000);
  resetFunc();  //call reset
}

void setup(){                     // Setup al subsystems
  if(DEBUG == 1){
    Serial.begin(9600);           //Set the serial baud rate.
  }
  if (!SD.begin(SDchipSelect)) {
    if(DEBUG){
      Serial.println("Card failed, or not present");
    }
    //return;
  }else{
    SD_avail = 1;
    if(DEBUG){
      Serial.println("card initialized.");
    }
  }
  if(SD_avail > 0){
    READ_SETTINGS();                // First read the settings.
    WRITE_SETTINGS();               // Write them again, if the file wasn't there, or completely filled, it will be by now.
    for (uint8_t i = 0; i < 100; i++) {
      track_filename[5] = i/10 + '0';
      track_filename[6] = i%10 + '0';
      if (! SD.exists(track_filename)) {
        break;  // leave the loop!
      }
    }
    if(DEBUG){
      Serial.print("Found track_filename to use: ");
      Serial.println(track_filename);
    }
    GPX_WRITE_TRACKLOG_HEADER();
  }
    if(DEBUG){
      Serial.println("Starting GPS.");
    }
  Serial1.begin(9600);            //Set the GPS baud rate.

  pinMode(xPin, INPUT);           // Setup pins for joystick
  pinMode(yPin, INPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  
  Wire.begin();                   // setup compas
  Wire.beginTransmission(compas_id);
  Wire.write(0x02);               //select mode register
  Wire.write(0x00);               //continuous measurement mode
  Wire.endTransmission();
  
  delay(50);
  
  display.begin(SSD1306_SWITCHCAPVCC); // setup oled
  display.clearDisplay(); // Clear display
  display.drawBitmap(0, 0,  LOGO_bits, 128, 64, 1);
  display.display();
  delay(3000);
  display.clearDisplay();
}
 
void loop(){
  int waitCount=0;
  while (Serial1.available())
  {
    if(gps.encode(Serial1.read())){           // Check the GPS data
      if(!gps.location.isValid()){ // Display waiting for fix...
        display.clearDisplay();
        display.setTextSize(2);
        display.setTextColor(WHITE);
        display.setCursor(0,0);
        display.println("Waiting");
        display.println("for fix");
        waitCount++;
        switch (waitCount) {
          case 1:
            display.println(".");
            break;
          case 2:
            display.println("..");
            break;
          case 3:
            display.println("...");
            break;
          case 4:
            display.println("....");
            waitCount=0;
            break;
        }
        if(DEBUG){
          Serial.print("waitCount: ");
          Serial.println(waitCount);
          Serial.print("Satellies found: ");
          Serial.println(gps.satellites.value());
        }
        display.display();
        delay(250);
      }else{
        xPosition = analogRead(xPin);
        yPosition = analogRead(yPin);
        buttonState = digitalRead(buttonPin);

        year=gps.date.year();
        month=gps.date.month();
        day=gps.date.day();
        hour=gps.time.hour();
        minute=gps.time.minute();
        second=gps.time.second();
        
        if(buttonState == 0){
          MENU();
        }else{
          display.clearDisplay(); // Clear display
          display.setTextSize(1);
          display.setTextColor(WHITE);
          if (fix_age > 5000){
            display.setCursor(120,0);
            display.print("X");
          }
          slat = String(gps.location.lat(),6);                               // Convert coordinates to string
          slng = String(gps.location.lng(),6);
          int latDot=slat.indexOf('.');                       // Since the Degrees can have lenght up to 3 digits, find the dot
          int lngDot=slng.indexOf('.');
          
          int latdec=slat.substring(0,latDot).toInt();                      //Select the decimal (part before dot)
          float latmin=(slat.substring(latDot+1,9).toFloat()/1000000)*60;   //Calculate the remainder as 0.xxxxxx and make it minutes
          int lngdec=slng.substring(0,lngDot).toInt();
          float lngmin=(slng.substring(lngDot+1,9).toFloat()/1000000)*60;
        
          if(DEBUG){
            Serial.print("Lat: ");
            Serial.print(latdec);
            Serial.print(" ");
            Serial.print(latmin,3);
            Serial.print(" Lng: ");
            Serial.print(lngdec);
            Serial.print(" ");
            Serial.println(lngmin,3);
            Serial.print("Satellites found: ");
            Serial.println(gps.satellites.value());
          }
          display.setCursor(0,16);
          display.print("Lat: ");
          display.print(gps.location.rawLat().negative ? "S" : "N");        // Negative is south, positive north
          display.print(latdec);
          display.print(" ");
          display.print(latmin,3);

          display.setCursor(0,24);
          display.print("Lng: ");
          display.print(gps.location.rawLng().negative ? "W" : "E");        // Negative is west, positive east
          display.print(lngdec);
          display.print(" ");
          display.print(lngmin,3);

          display.setCursor(0,32);
          display.print("Altitude: ");
          display.println(gps.altitude.meters());

          display.setCursor(0,40);
          display.print("Satellites: ");
          display.print(gps.satellites.value());

          if(SD_avail == 0){
            display.setCursor(0,48);
            display.print("No SD-card found!");
          }
          
          COMPAS();
          DISPLAY_TIME();
          display.display();  // rebuild display
          String datetime = String(year) + "-" + String(month) + "-" + String(day) + "T" + String(hour) + ":" + String(minute) + ":" + String(second) + "Z";
          gpx_curtime = String(hour) + String(minute) + String(second);
          if(gpx_lasttime < gpx_curtime){
            GPX_WRITE_TRACKLOG_POINT(String(gps.location.lat(),6), String(gps.location.lng(),6), datetime, String(gps.altitude.meters()), String(gps.hdop.value()));
            gpx_lasttime = gpx_curtime;
          }
        }
        delay(50);
      }
    }
  }
}
